<?php get_header(); ?>
	<div class="main" id="content">
		<div class="g960" >
			<div id="contact">
				<div class="content">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div class="g460 inside">
						<img src="<?php echo get_post_image($post->ID, 'full',false)?>" width="436" height="386" title="<?php the_title(); ?>">
					</div>
					<div class="g300 ml40">
						<?php the_title('<h1>','</h1>'); ?>
						<?php echo do_shortcode('[contact-form-7 id="85" title="Contacto"]'); ?>
					</div>
					<?php endwhile; else: ?>
					<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
					<?php endif; ?>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>