<?php get_header(); ?>
	<div class="main" id="content">
		<div class="g960" >
			<div id="banner">
				<div id="wrapper">  
				    <div class="slider-wrapper theme-default">
				    	<?php query_posts("page_id=2"); ?>
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
						<?php $fotos = get_post_images_full($post->ID,'full'); ?>
						<?php $fotos = get_post_images_full($post->ID,'description'); ?>
				        <div id="slider" class="nivoSlider">
				            <?php foreach($fotos as $foto): ?>
							<a href="<?php echo $foto['description']; ?>" target="_blank">
								<img src="<?php echo $foto['full']; ?>">
							</a>
							<?php endforeach;?>
				        </div>
				        <?php endwhile; endif; wp_reset_query(); ?> 
				    </div>
				</div>
			</div>
			<div class="clear"></div>

			<div class="relative" id="band">
			<?php query_posts("page_id=4"); ?>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
				<img src="<?php echo get_post_image($post->ID, 'full',false)?>" width="940" height="450">
				<div class="content">
					<?php the_title('<h1>','</h1>'); ?>
					<?php the_content(); ?>

					<a href="<?php echo get_category_link(2); ?>" class="btn-more-info">More Information</a>		
				</div>
			<?php endwhile; endif; wp_reset_query(); ?> 	
			</div>
			<div class="clear"></div>

			<div id="photos">
				<div class="content">
				<h1><?php echo get_cat_name(10); ?></h1>	
				<?php $i=0; ?>
				<?php $cats = get_categories('child_of=10&hide_empty=0'); ?>
				<?php foreach($cats as $cat): $i++; ?>
					<div class="g220 <?php echo($i==1)?'inside':'ml13'; ?>">
						<a href="<?php echo get_category_link($cat->term_id); ?>">
							<img src="<?php echo z_taxonomy_image_url($cat->term_id); ?>" width="196" height="146" />
						</a>
						<h3><a href="<?php echo get_category_link($cat->term_id); ?>"><?php echo $cat->cat_name; ?></a></h3>
					</div>
					
					<?php if($i==4): $i=0; ?>
					<div class="clear h20px"></div>
					<?php endif; ?>

				<?php endforeach; wp_reset_query(); ?>
				<!--	
				<?php query_posts("page_id=7"); ?>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
					<?php the_title('<h1>','</h1>'); ?>
					<?php $i=0; $j=0; ?>
					<?php $fotos = get_post_images_full($post->ID,'full'); ?>
					<?php $fotos = get_post_images_full($post->ID,'description'); ?>
					<?php foreach($fotos as $foto): $i++; $j++; ?>
					
					<?php if($j<=10): ?>
					<div class="g180 <?php echo($i==1)?'inside':'ml10'; ?>">
						<a class="lightbox" href="<?php echo $foto['full']; ?>" rel="group2" title="<?php echo $foto['description']; ?>">
							<img src="<?php echo $foto['full']; ?>" width="146" height="146" title="<?php the_title(); ?>">
						</a>
					</div>
					<?php endif; ?>

					<?php if($i==5): $i=0; ?>
					<div class="clear h20px"></div>
					<?php endif; ?>

					<?php endforeach;?>
				
				<?php endwhile; endif; wp_reset_query(); ?>
				--> 
				</div>	
			</div>
			<div class="clear"></div>

			<div id="schedule">
				<div class="content">
					<h1><?php echo get_cat_name(3); ?></h1>

					<?php $args = array('category' => 3,'numberposts' => -1, 'orderby' => 'date', 'order' => 'ASC' ); ?>
					<?php $myposts = get_posts( $args ); ?>
					<table>
					<?php foreach( $myposts as $post ) : setup_postdata($post); ?>
						<tr>
						  <td><?php echo get_post_meta($post->ID,'fecha',true); ?></td>
						  <td><?php echo get_post_meta($post->ID,'lugar',true); ?></td> 
						  <td><?php echo get_post_meta($post->ID,'ciudad',true); ?></td>
						  <td>
						  	<?php if(get_post_meta($post->ID,'liga',true)): ?>
								<a href="<?php echo get_post_meta($post->ID,'liga',true); ?>" class="button" target="_blank">Buy</a> 
							<?php endif; ?>
						  </td>
						</tr>
					<?php endforeach; wp_reset_query(); ?>
					</table>
				</div>
			</div>
			<div class="clear"></div>

			<div id="music">
				<div class="content">
					<div class="g740 inside">
						<h1><?php echo get_cat_name(9); ?></h1>
					</div>

					<?php $args = array('category' => 9,'numberposts' => 1, 'orderby' => 'date', 'order' => 'ASC' ); ?>
					<?php $myposts = get_posts( $args ); ?>
					<?php foreach( $myposts as $post ) : setup_postdata($post); ?>

					<div class="g860 inside">
						<a href="<?php echo get_post_meta($post->ID,'link-iTunes',true); ?>" target="_blank" class="itunes"></a>
					</div>
					<div class="clear"></div>
					<div class="g280 inside">
						<img src="<?php echo get_post_image($post->ID, 'thumbnail',false)?>" width="254" height="254">
					</div>
					<div class="g580">
						<?php the_content(); ?>
					</div>
					<div class="clear h20px"></div>
						<?php echo get_post_meta($post->ID,'inf-adicional',true); ?>
					<div class="clear h20px"></div>

					<?php endforeach; wp_reset_query(); ?>

					<div class="clear"></div>
				</div>
			</div>
			<div class="clear"></div>

			<div id="videos">
				<div class="header">
					<h1><?php echo get_cat_name(4); ?></h1>
				</div>
				<div class="content">
					<?php $args = array('category' => 4,'numberposts' => 3, 'orderby' => 'date', 'order' => 'DESC' ); ?>
					<?php $myposts = get_posts( $args ); ?>
					<?php $i=0; ?>
					<?php foreach( $myposts as $post ) : setup_postdata($post); $i++; ?>
					<div class="g280 <?php echo($i==1)?'':'ml20'; ?>">
						<a class='youtube' href="<?php echo get_post_meta($post->ID,'video',true); ?>" title="<?php the_title(); ?>">
							<img src="<?php echo get_post_image($post->ID, 'full',false)?>" width="260" height="170">
						</a>
					</div>
					<?php endforeach; wp_reset_query(); ?>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php get_footer(); ?>