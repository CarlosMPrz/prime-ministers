<?php
add_filter('excerpt_length', 'my_excerpt_length');
function my_excerpt_length($length) {
	if(in_category(7)) {
        return 13;
    } else {
		return 80;
    }
}

if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'name' => 'home',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '',
		'after_title' => '',
	));
if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'name' => 'footer',
		'before_widget' => '<span class="copyright">',
		'after_widget' => '</span>',
		'before_title' => '',
		'after_title' => '',
	));
	
//thumbnail, medium, large or full
function get_post_image($post_id, $size = 'thumbnail', $echo = true, $class = ''){
	$args = array( 
		'post_parent' => $post_id,
		'post_type'   => 'attachment', 
		'post_mime_type' => 'image',
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);
	$pictures = get_children($args);
	$i = 0;
	if(!empty($pictures)){
		foreach($pictures as $picture_id=>$picture){
			if($i == 0){
				$src = wp_get_attachment_image_src($picture_id, $size, false);
				$src = $src[0];
				$image = '<img src="'.$src.'" alt="'.get_the_title($post_id).'" class="'.$class.'" />';
			}
			$i++;
		}
	}
	if($echo)
		echo $image;
	else
		return $src;
}

function get_post_images($post_id, $size = 'thumbnail'){
	$args = array( 
		'post_parent' => $post_id,
		'post_type'   => 'attachment', 
		'post_mime_type' => 'image',
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);
	$pictures = get_children($args);
	$img = array();
	if(!empty($pictures)){
		foreach($pictures as $picture_id=>$picture){
			$src = wp_get_attachment_image_src($picture_id, $size, false);
			$thumb_src = wp_get_attachment_image_src($picture_id, 'thumbnail', false);
			$thumb_src = $thumb_src[0];
			$src = $src[0];
			$temp = array(
				/*'img' => '<img src="'.$src.'" alt="'.get_the_title($post_id).'" />',*/
				'img' => $src,
				'thumb' => $thumb_src,
				'description' => $picture->post_content,
				'title' => $picture->post_title
			);
			$img[] = $temp;
		}
	}
	return $img;
}

function get_post_attachment($post_id){
	$args = array(
			'post_parent' => $post_id,
			'post_status' => 'inherit',
			'post_type' => 'attachment',
			'post_mime_type' => 'application/pdf',
			'order' => 'ASC',
			'orderby' => 'menu_order ID'
	);
	$attachments = get_children($args);
	$data = array();
	if ($attachments) {
		foreach ($attachments as $attachment) {
			//echo apply_filters('the_title', $attachment->post_title);
			$data[] = wp_get_attachment_url($attachment->ID, false);
		}
	}
	return $data;
}
function register_my_menus() {
	register_nav_menus(
		array( 'header-menu' => __( 'Header Menu' ),
			'extra-menu' => __( 'Pre header Menu' )
			)
	);
}
add_action( 'init', 'register_my_menus' );

function get_post_images_full($post_id, $size = 'full'){
	$args = array( 
		'post_parent' => $post_id,
		'post_type'   => 'attachment', 
		'post_mime_type' => 'image',
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);
	$pictures = get_children($args);
	$img = array();
	if(!empty($pictures)){
		foreach($pictures as $picture_id=>$picture){
			$src = wp_get_attachment_image_src($picture_id, $size, false);
			$full_src = wp_get_attachment_image_src($picture_id, 'full', false);
			$full_src = $full_src[0];
			$src = $src[0];
			$temp = array(
				/*'img' => '<img src="'.$src.'" alt="'.get_the_title($post_id).'" />',*/
				'img' => $src,
				'full' => $full_src,
				'description' => $picture->post_content,
				'title' => $picture->post_title
			);
			$img[] = $temp;
		}
	}
	return $img;
}
function smart_category_top_parent_id ($catid) {
    while ($catid) {
        $cat = get_category($catid);
        $catid = $cat->category_parent; 
        $catParent = $cat->cat_ID;
    }
    return $catParent;
}

/* Agregado por iTrends */

function prime_woocommerce_scripts() {
	wp_enqueue_style( 'prime-woocommerce', get_template_directory_uri().'/woocommerce/woocommerce.css' );
}

add_action( 'wp_enqueue_scripts', 'prime_woocommerce_scripts' );

add_theme_support( 'woocommerce' );

?>