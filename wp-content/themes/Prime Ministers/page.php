<?php get_header(); ?>
	<div class="main" id="content">
		<div class="g960" >
			<div id="default">
				<div class="content">
				<?php if (have_posts()) : while (have_posts()) : the_post(); $i++; ?>
				
					<?php the_title('<h1>','</h1>'); ?>
					<?php the_content(); ?>

				<?php endwhile; else: ?>
				<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
				<?php endif; ?>

				<div class="clear h30px"></div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>