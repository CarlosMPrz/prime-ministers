<?php get_header(); ?>
	<div class="main" id="content">
		<div class="g960" >
			<div id="news">
				<div class="content">
					<h1><?php echo single_cat_title(); ?></h1>

					<?php
					    $wp_query->set('orderby', 'menu_order');
					    $wp_query->set('order', 'ASC');
					    $wp_query->get_posts();
					?>
					<?php $i=0; ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); $i++; ?>
					
					<div class="g440 <?php echo($i==1)?'inside':'ml20'; ?>">
						<div class="g220 inside">
							<a href="<?php the_permalink(); ?>">
								<img src="<?php echo get_post_image($post->ID, 'thumbnail',false)?>" width="196" height="176">
							</a>
						</div>
						<div class="g220">
							<?php the_title('<h2>','</h2>'); ?>
							<p class="h90px overflow"><?php echo substr(get_the_excerpt(), 0,75); ?></p>
							<a href="<?php the_permalink(); ?>" class="btn-more-info">More Information</a>
						</div>
					</div>

					<?php if($i==2): $i=0; ?>
					<div class="clear h50px"></div>
					<?php endif; ?>

					<?php endwhile; else: ?>
					<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
					<?php endif; ?>

					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>