<?php get_header(); ?>
	<div class="main" id="content">
		<div class="g960" >
			<div id="default">
				<div class="content">

					<?php if (have_posts()) : while (have_posts()) : the_post(); $i++; ?>
					
					<div class="g320 inside">
						<img src="<?php echo get_post_image($post->ID, 'full',false)?>" width="296" height="221">
					</div>
					<div class="g580">
						<?php the_title('<h1>','</h1>'); ?>
						<?php the_content(); ?>
						<div class="clear h30px"></div>
						<div id="comments">
							<?php comments_template(); ?>
						</div>
					</div>

					<?php endwhile; else: ?>
					<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
					<?php endif; ?>

					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>