<?php 

	$category = get_the_category($post->ID);
	$cat_id = $category[0]->cat_ID;
	$top_level_cat = smart_category_top_parent_id ($cat_id);

	if($top_level_cat == 10) {
		include(TEMPLATEPATH.'/category-gallery-esp.php');
	} else {
		include(TEMPLATEPATH.'/category-default.php');
	} 
	 
?>