<?php get_header(); ?>
	<div class="main" id="content">
		<div class="g960" >
			<div id="default">
				<div class="content">
				<?php woocommerce_content(); ?>
				<div class="clear h30px"></div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>