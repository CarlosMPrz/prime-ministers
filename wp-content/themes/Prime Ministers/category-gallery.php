<?php get_header(); ?>
	<div class="main" id="content">
		<div class="g960" >
			<div id="photos">
				<div class="content" id="id-7">
					<?php $i=0; ?>
					<?php $cats = get_categories('child_of=10&hide_empty=0'); ?>
					<?php foreach($cats as $cat): $i++; ?>
						<div class="g220 <?php echo($i==1)?'inside':'ml13'; ?>">
							<a href="<?php echo get_category_link($cat->term_id); ?>">
								<img src="<?php echo z_taxonomy_image_url($cat->term_id); ?>" width="196" height="146" />
							</a>
							<h3><a href="<?php echo get_category_link($cat->term_id); ?>"><?php echo $cat->cat_name; ?></a></h3>
						</div>
						
						<?php if($i==4): $i=0; ?>
						<div class="clear h20px"></div>
						<?php endif; ?>

					<?php endforeach; wp_reset_query(); ?>	
					<div class="clear"></div>
				</div>	
			</div>
		</div>
	</div>
<?php get_footer(); ?>