<?php get_header(); ?>	
	<div class="main" id="content">
		<div class="g960" >
			<div id="music">
				<div class="content">
					<?php
					    $wp_query->set('orderby', 'menu_order');
					    $wp_query->set('order', 'ASC');
					    $wp_query->get_posts();
					?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<div class="g860 inside">
						<a href="<?php echo get_post_meta($post->ID,'link-iTunes',true); ?>" target="_blank" class="itunes"></a>
					</div>
					<div class="clear h25px"></div>
					<div class="g280 inside">
						<img src="<?php echo get_post_image($post->ID, 'thumbnail',false)?>" width="254" height="254">
					</div>
					<div class="g580">
						<?php the_content(); ?>
					</div>
					<div class="clear h20px"></div>
						<div id="inf-adicional">
							<?php echo get_post_meta($post->ID,'inf-adicional',true); ?>
						</div>
					<div class="clear h20px"></div>

					<?php endwhile; else: ?>
					<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
					<?php endif; ?>

					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>