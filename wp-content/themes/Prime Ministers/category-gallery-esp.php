<?php get_header(); ?>
	<div class="main" id="content">
		<div class="g960" >
			<div id="photos">
				<div class="content" id="id-7">
					<h1><?php echo single_cat_title(); ?></h1>

					<?php $i=0; ?>
					<?php $fotos = get_post_images_full($post->ID,'full'); ?>
					<?php $fotos = get_post_images_full($post->ID,'description'); ?>
					<?php foreach($fotos as $foto): $i++; ?>
					
					<div class="g220 <?php echo($i==1)?'inside':'ml13'; ?>">
						<a class="lightbox" href="<?php echo $foto['full']; ?>" rel="group2" title="<?php echo $foto['description']; ?>">
							<img src="<?php echo $foto['full']; ?>" width="196" height="146">
						</a>
					</div>

					<?php if($i==4): $i=0; ?>
					<div class="clear h20px"></div>
					<?php endif; ?>

					<?php endforeach;?>

					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>