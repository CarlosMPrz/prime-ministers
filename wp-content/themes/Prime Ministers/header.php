<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/css/reset.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/css/text.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/css/980_48_20.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/css/layout.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/style.css" />
<?php wp_enqueue_script("jquery"); ?>
<?php wp_head(); ?>

<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/img/favicon.ico">
<script src="<?php bloginfo('template_url'); ?>/js/build/jquery.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/build/jquery-migrate-1.2.1.min.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/build/mediaelementplayer.min.css" />
<script src="<?php bloginfo('template_url'); ?>/js/script.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/modalbox.css" />
<script src="<?php bloginfo('template_url'); ?>/js/modaljquery.colorbox.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/default.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/nivo-slider.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.nivo.slider.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/build/mediaelement-and-player.min.js"></script>

<script>
	jQuery(document).ready(function(){
		jQuery(".lightbox").colorbox({rel:'group2', transition:"fade"});
		jQuery(".youtube").colorbox({iframe:true, innerWidth:750, innerHeight:500});
		jQuery('#slider').nivoSlider({effect: 'fade', controlNav: false });
	});
</script>

</head>
<body id="bg" data-speed="2.3" data-type="background" <?php body_class($class); ?>>
	<div class="main">
		<div class="g300">
			<div class="clear h10px"></div>
			<ul class="language inline">
				<li><a href="http://esp.primeministersband.com/">Español</a></li>
				<li><a href="http://primeministersband.com/">English</a></li>
			</ul>
		</div> 
		<div class="g660">
			<div class="clear h10px"></div>
			<?php wp_nav_menu( array( 'theme_location' => 'extra-menu','container_class' => 'shop_menu' )); ?>
		</div>
	</div>		
	<div class="main" id="header">
		<div class="g960">
			<div class="clear h120px"></div>
			<div class="g380 inside">
				<a href="<?php bloginfo('url') ?>">
					<img src="<?php bloginfo('template_url'); ?>/img/logo.png" width="350" height="155" title="Prime Ministers">
				</a>
			</div>
			<div class="g580">
			<ul class="social inline">
				<li class="social last">
					<a href="<?=get_post_meta(2,'facebook', true)?>" target="_blank" class="social fb"></a>
					<a href="<?=get_post_meta(2,'twitter', true)?>" target="_blank" class="social tw"></a>
					<a href="<?=get_post_meta(2,'instagram', true)?>" target="_blank" class="social ins"></a>
					<a href="<?=get_post_meta(2,'youtube', true)?>" target="_blank" class="social yt"></a>
					<a href="<?=get_post_meta(2,'iTunes', true)?>" target="_blank" class="social it"></a>
					<a href="<?=get_post_meta(2,'spotify', true)?>" target="_blank" class="social spotify"></a>
					<a href="<?=get_post_meta(2,'googleplay', true)?>" target="_blank" class="social gp"></a>
					<a href="<?=get_post_meta(2,'reverbnation', true)?>" target="_blank" class="social reverbnation"></a>
				</li>
			</ul>
			</div>
			<div class="clear h5px"></div>
			<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => false, 'menu_class' => 'inline main-nav inline' )); ?>
		</div>
	</div>
	<div class="clear"></div>