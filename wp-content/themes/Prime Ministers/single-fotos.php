<?php get_header(); ?>
	<div class="main" id="content">
		<div class="g960" >
			<div id="photos">
				<div class="content" id="id-<?php echo get_the_ID(); ?>">
					<?php $i=0; ?>
					<?php $fotos = get_post_images_full($post->ID,'full'); ?>
					<?php $fotos = get_post_images_full($post->ID,'description'); ?>
					<?php foreach($fotos as $foto): $i++; ?>
					
					<div class="g180 <?php echo($i==1)?'inside':'ml10'; ?>">
						<a class="lightbox" href="<?php echo $foto['full']; ?>" rel="group2" title="<?php echo $foto['description']; ?>">
							<img src="<?php echo $foto['full']; ?>" width="146" height="146">
						</a>
					</div>

					<?php if($i==5): $i=0; ?>
					<div class="clear h20px"></div>
					<?php endif; ?>

					<?php endforeach;?>

					<div class="clear"></div>
				</div>	
			</div>
		</div>
	</div>
<?php get_footer(); ?>