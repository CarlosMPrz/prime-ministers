<?php get_header(); ?>
	<div id="full-band">
		<div class="main" id="content">
			<div class="g960">
				<div id="band-int">
					<div class="content">	
						<div class="row-1">
							<div class="header"></div>
							<div class="content">
								<?php echo category_description(); ?> 
								<div class="clear"></div>
							</div>
							<div class="footer"></div>	
						</div>

						<?php $args = array('category' => 2,'numberposts' => -1, 'orderby' => 'date', 'order' => 'ASC' ); ?>
						<?php $myposts = get_posts( $args ); ?>
						<?php $i=1; ?>
						<?php foreach( $myposts as $post ) : setup_postdata($post); $i++;?>
						
						<div class="row-<?php echo $i; ?>" style="background:url(<?php echo get_post_image($post->ID, 'full',false)?>);">
							<div class="content">
								<?php the_title('<h1>','</h1>'); ?>
								<div class="clear"></div>
								<?php the_content(); ?>
							</div>
						</div>
						<?php endforeach; wp_reset_query(); ?>
					</div>	
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
<?php get_footer(); ?>