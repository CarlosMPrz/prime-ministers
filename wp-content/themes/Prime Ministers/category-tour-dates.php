<?php get_header(); ?>
	<div class="main" id="content">
		<div class="g960" >
			<div id="schedule">
				<div class="content">
					<h1><?php echo single_cat_title(); ?></h1>
					<?php
					    $wp_query->set('orderby', 'menu_order');
					    $wp_query->set('order', 'ASC');
					    $wp_query->get_posts();
					?>
					<table>
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<tr>
						  <td><?php echo get_post_meta($post->ID,'fecha',true); ?></td>
						  <td><?php echo get_post_meta($post->ID,'lugar',true); ?></td> 
						  <td><?php echo get_post_meta($post->ID,'ciudad',true); ?></td>
						  <td>
						  	<?php if(get_post_meta($post->ID,'liga',true)): ?>
								<a href="<?php echo get_post_meta($post->ID,'liga',true); ?>" class="button" target="_blank">Buy</a> 
							<?php endif; ?>
						  </td>
						</tr>
						<?php endwhile; else: ?>
						<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
						<?php endif; ?>											
					</table>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>