<?php get_header(); ?>
	<div class="main" id="content">
		<div class="g960" >
			<div id="videos-int">
				<div class="content">
					<?php
					    $wp_query->set('orderby', 'menu_order');
					    $wp_query->set('order', 'DESC');
					    $wp_query->get_posts();
					?>
					<?php $i=0; ?>
					<div class="bg">
						<?php if (have_posts()) : while (have_posts()) : the_post(); $i++; ?>
						
						<div class="g280 <?php echo($i==1)?'':'ml20'; ?>">
							<a class='youtube' href="<?php echo get_post_meta($post->ID,'video',true); ?>" title="<?php the_title(); ?>">
								<img src="<?php echo get_post_image($post->ID, 'full',false)?>" width="260" height="170">
							</a>
							<div class="clear h80px"></div>
							<h2>
								<a class='youtube' href="<?php echo get_post_meta($post->ID,'video',true); ?>" title="<?php the_title(); ?>">
									<?php the_title(); ?>
								</a>
							</h2>
						</div>
						<?php if($i==3): $i=0; ?>
						<div class="clear h95px"></div>
						<?php endif; ?>
						
						<?php endwhile; else: ?>
						<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
						<?php endif; ?>
						<div class="clear"></div>	
					</div>

				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>